" Plugins {{{
call plug#begin('~/.local/share/nvim/plugged')

" general
Plug 'dracula/vim', {'as': 'dracula'}
Plug 'andreypopp/vim-colors-plain', {'as': 'plain'}
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'honza/vim-snippets'
Plug 'sgur/vim-editorconfig'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

Plug 'w0rp/ale'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'mvolkmann/vim-react'
Plug 'sheerun/vim-polyglot'

Plug 'majutsushi/tagbar'

Plug 'dhruvasagar/vim-table-mode'

Plug 'xolox/vim-misc'

Plug 'kshenoy/vim-signature'

Plug 'ryanoasis/vim-devicons'

Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'for': ['javascript', 'css', 'less', 'scss', 'json', 'markdown', 'typescript'] }

Plug 'benmills/vimux'

Plug 'janko-m/vim-test'

call plug#end()
" }}}

" General config {{{
" https://stackoverflow.com/questions/33380451/is-there-a-difference-between-syntax-on-and-syntax-enable-in-vimscript
if !exists('g:syntax_on')
    syntax enable
endif

if &compatible
    set nocompatible
endif

set encoding=UTF-8
set expandtab
set tabstop=4
set shiftwidth=4
set showcmd
set showtabline=2
set wildmenu
set showmatch
set ruler
set incsearch
set visualbell
set backspace=2
set laststatus=2
set textwidth=80
set title
set laststatus=2
set clipboard=unnamed
set colorcolumn=99
" Show invisibles
set list
set showbreak=↪\
set listchars=tab:→\ ,eol:↲,nbsp:␣,trail:•,extends:⟩,precedes:⟨
set foldmethod=marker

let mapleader = " "

set undofile
set undodir=$HOME/.vim/undo

" }}}

" Plugin configs {{{
" Test config {{{
let test#strategy= "neovim"
" h4 stuff :)
"    note: h4 == h[onor]
" vim-test transformation to run nose tests via `make singletest`.
" If a command looks like "nosetests ...", transform it to "make singletest NOSEARGS='...'"
function! HonorTransform(cmd) abort
    if a:cmd =~ '^nosetests '
        let l:cmd_sans_nosetests = "-s ".substitute(a:cmd, '^nosetests ', '', '')
        let l:new_cmd = 'make singletest TEST_PROCESSES=0 TEST_DB_COUNT=1 NOSEARGS='.shellescape(cmd_sans_nosetests)
    else
        let l:new_cmd = a:cmd
    endif
    return l:new_cmd
endfunction

" Force use of nosetest over pytest
let test#python#pytest#file_pattern = '\vMATCH_NOTHING_AT_ALL$'
let test#python#nose#file_pattern = '\v(^|[\b_\.-])[Tt]est.*\.py$'
let g:test#custom_transformations = {'honor': function('HonorTransform')}
let g:test#transformation = 'honor'

" let test#custom_runners = {'honorjs': ['h4runner']}
" }}}

" Prettier config {{{
let g:prettier#exec_cmd_async = 1
let g:prettier#autoformat = 0
autocmd BufWritePre *.ts,*.tsx,*.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html PrettierAsync
" }}}

" Coc config {{{

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Use K for show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction
" if hidden is not set, TextEdit might fail.
set hidden
set updatetime=300
set shortmess+=c
set signcolumn=yes
nmap <silent> <leader>ni <Plug>(coc-diagnostic-next)
nmap <silent> <leader>np <Plug>(coc-diagnostic-prev)
" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> for trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> for confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

let g:coc_node_path=exepath('node')

let s:LSP_CONFIG = [
      \ ['tsserver', {
      \   'command': exepath('tsserver'),
      \   'args': ['lsp'],
      \   'filetypes': ['javascript', 'javascript.jsx'],
      \   'initializationOptions': {},
      \   'requireRootPattern': 1,
      \   'settings': {},
      \ }],
      \ ['bash', {
      \   'command': exepath('bash-language-server'),
      \   'args': ['start'],
      \   'filetypes': ['sh', 'bash'],
      \   'ignoredRootPaths': ['~']
      \ }],
      \ ]

call coc#config('coc.preferences', {
      \ 'colorSupport': 0,
      \ 'hoverTarget': 'float',
      \ })

call coc#config('suggest', {
      \ 'autoTrigger': 'always',
      \ 'noselect': 0,
      \ 'echodocSupport': 1,
      \ 'floatEnable': 1,
      \ })

call coc#config('signature', {
      \ 'target': 'float',
      \ })

call coc#config('diagnostic', {
      \ 'errorSign': '×',
      \ 'warningSign': '●',
      \ 'infoSign': '!',
      \ 'hintSign': '?',
      \ 'messageTarget': 'float',
      \ 'displayByAle': 0,
      \ 'refreshOnInsertMode': 1
      \ })

call coc#config('python', {
      \ 'linting': {
      \   'pylintUseMinimalCheckers': 0
      \   }
      \ })

call coc#config('git', {
      \ 'enableGutters': 1,
      \ 'addedSign.text':'▎',
      \ 'changedSign.text':'▎',
      \ 'removedSign.text':'◢',
      \ 'topRemovedSign.text': '◥',
      \ 'changeRemovedSign.text': '◢',
      \ })

call coc#config('coc.github', {
      \ 'filetypes': ['gitcommit', 'markdown.ghpull']
      \ })

let s:languageservers = {}
for [lsp, config] in s:LSP_CONFIG
  " COC chokes on emptykcommands https://github.com/neoclide/coc.nvim/issues/418#issuecomment-462106680"
  let s:not_empty_cmd = !empty(get(config, 'command'))
  if s:not_empty_cmd | let s:languageservers[lsp] = config | endif
endfor

if !empty(s:languageservers)
  call coc#config('languageserver', s:languageservers)
endif
" }}}

" Ale config {{{
let g:ale_set_signs = 0
let g:ale_lint_on_text_changed = 'normal'
let g:ale_lint_on_insert_leave = 1
" let g:ale_lint_delay = 0

let g:ale_fix_on_save = 1
let g:ale_set_loclist = 0
let g:ale_set_quickfix = 1
" let g:ale_virtualtext_cursor = 1
" let g:ale_virtualtext_prefix = ' '
let g:ale_list_window_size = 5
let g:ale_sign_error = '×'
let g:ale_sign_warning = g:ale_sign_error
let g:ale_sign_style_error = '●'
let g:ale_sign_style_warning = g:ale_sign_error
let g:ale_statusline_format = ['E•%d', 'W•%d', 'OK']
let g:ale_echo_msg_error_str='[ERROR]'
let g:ale_echo_msg_info_str='[INFO]'
let g:ale_echo_msg_warning_str='[WARNING]'
let g:ale_echo_msg_format = '%severity% %linter% -> [%code%] -> %s'
let g:ale_javascript_prettier_use_local_config = 1
let g:ale_linter_aliases = {
      \ 'mail': 'markdown',
      \ 'html': ['html', 'css']
      \}

let g:ale_linters = {
      \ 'javascript': ['eslint'],
      \ 'javascript.jsx': ['eslint'],
      \ 'typescript': ['eslint'],
      \}

" ESLint --fix is so slow to run it as part of the fixers, so I do this using a precommit hook or something else
let g:ale_fixers = {
      \   '*'         : ['remove_trailing_lines', 'trim_whitespace'],
      \   'markdown'  : ['prettier'],
      \   'javascript': ['prettier'],
      \   'typescript': ['prettier'],
      \   'css'       : ['prettier'],
      \   'json'      : ['prettier'],
      \   'scss'      : ['prettier'],
      \   'yaml'      : ['prettier'],
      \   'graphql'   : ['prettier'],
      \   'html'      : ['prettier'],
      \   'reason'    : ['refmt'],
      \   'python'    : ['isort', 'black',],
      \   'sh'        : ['shfmt'],
      \   'bash'      : ['shfmt'],
      \   'rust'      : ['rustfmt'],
      \   'go'        : ['gofmt'],
      \}

" Don't auto auto-format files inside `node_modules`, `forks` directory, minified files and jquery (for legacy codebases)
let g:ale_pattern_options_enabled = 1
let g:ale_pattern_options = {
      \   '\.min\.(js\|css)$': {
      \       'ale_linters': [],
      \       'ale_fixers': ['remove_trailing_lines', 'trim_whitespace']
      \   },
      \   'jquery.*': {
      \       'ale_linters': [],
      \       'ale_fixers': ['remove_trailing_lines', 'trim_whitespace']
      \   },
      \   'node_modules/.*': {
      \       'ale_linters': [],
      \       'ale_fixers': []
      \   },
      \   'package.json': {
      \       'ale_fixers': ['remove_trailing_lines', 'trim_whitespace']
      \   },
      \}
" }}}

" Fzf config {{{
nnoremap <silent> <C-p> :call Fzf_dev()<CR>

" ripgrep
if executable('rg')
  let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --follow --glob "!.git/*"'
  set grepprg=rg\ --vimgrep
  command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)
endif

" Files + devicons
function! Fzf_dev()
  let l:fzf_files_options = '--preview "bat --style=numbers,changes --color always {2..-1} | head -'.&lines.'"'

  function! s:files()
    let l:files = split(system($FZF_DEFAULT_COMMAND), '\n')
    return s:prepend_icon(l:files)
  endfunction

  function! s:prepend_icon(candidates)
    let l:result = []
    for l:candidate in a:candidates
      let l:filename = fnamemodify(l:candidate, ':p:t')
      let l:icon = WebDevIconsGetFileTypeSymbol(l:filename, isdirectory(l:filename))
      call add(l:result, printf('%s %s', l:icon, l:candidate))
    endfor

    return l:result
  endfunction

  function! s:edit_file(item)
    let l:pos = stridx(a:item, ' ')
    let l:file_path = a:item[pos+1:-1]
    execute 'silent e' l:file_path
  endfunction

  call fzf#run({
        \ 'source': <sid>files(),
        \ 'sink':   function('s:edit_file'),
        \ 'options': '-m ' . l:fzf_files_options,
        \ 'down':    '40%' })
endfunction
" }}}

" }}}

" Color & theme {{{
set bg=dark
colorscheme plain

if has("termguicolors")
    set termguicolors
endif
" }}}

" Nvim specific {{{
if has('nvim')
  " neovim let's me adjust pane sizes w mouse, don't judge :)
  set mouse=a
endif
" }}}

" Brace expansion {{{
noremap {<CR> {<CR>}<Esc>O
inoremap {; {<CR>};<Esc>O
inoremap {, {<CR>},<Esc>O
inoremap ({ ({<CR>})<Esc>O
inoremap {) {<CR>});<Esc>O
" }}}

" Some remaps {{{
" go up visually, but when given a count, go up by line count :)
nnoremap <expr> j v:count ? 'j' : 'gj'
nnoremap <expr> k v:count ? 'k' : 'gk'
nnoremap z] zo]z
nnoremap N Nzz
nnoremap n nzz
nnoremap <tab> :tabnext<CR>
nnoremap <leader>b :ls<cr>:b<space>
nnoremap <leader>tn :TestNearest<CR>
nnoremap <expr><silent> <Bar> v:count == 0 ? '<C-W>v<C-W><Right>' : ':<C-U>normal! 0'.v:count.'<Bar><CR>'
nnoremap <expr><silent> _ v:count == 0 ? '<C-W>s<C-W><Down>'  : ':<C-U>normal! '.v:count.'_<CR>'
nnoremap <C-p> :FZF<CR>
nnoremap π     :Rg<CR>
nnoremap <Left> :bprev<CR>
nnoremap <Right> :bnext<CR>
nnoremap <leader>s :%s/\<<C-r><C-w>\>/<C-r><C-w>
nnoremap <leader>l /\<<C-r><C-w>\><CR>N
" }}}

" Statusline {{{
set statusline=%{FileNameWithParent()}       "tail of the filename
set statusline+=\ %{get(g:,'coc_git_status','')}%{get(b:,'coc_git_status','')}%{get(b:,'coc_git_blame','')}
set statusline+=\ %m      "modified flag
set statusline+=\ %=      "left/right separator
set statusline+=\ %c,     "cursor column
set statusline+=\ %l/%L   "cursor line/total lines
set statusline+=\ %P    "percent through file
function! FileNameWithParent()
    let dirname = expand('%:p:h:t')
    let fname = expand('%:t')
    return dirname . '/' . fname
endfunction
" }}}
