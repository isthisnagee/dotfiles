source ~/.zplug/init.zsh
#
# Make sure you use double quotes
zplug "zsh-users/zsh-history-substring-search"


# Prohibit updates to a plugin by using the "frozen:" tag
zplug "k4rthik/git-cal", as:command, frozen:1

# Grab binaries from GitHub Releases
# and rename using the "rename-to:" tag
zplug "junegunn/fzf-bin", \
    as:command, \
    from:gh-r, \
    rename-to:fzf, \
    use:"*darwin*amd64*"

# Support oh-my-zsh plugins and the like
zplug "plugins/git",   from:oh-my-zsh, if:"(( $+commands[git] ))"
zplug "lib/clipboard", from:oh-my-zsh, if:"[[ $OSTYPE == *darwin* ]]"

# Also supports prezto plugins
# Defers the loading of a package
# e.g., zsh-syntax-highlighting must be loaded
# after executing compinit command and sourcing other plugins
zplug "zsh-users/zsh-syntax-highlighting", defer:3
zplug "zsh-users/zsh-history-substring-search", as:plugin
zplug "romkatv/powerlevel10k", use:powerlevel10k.zsh-theme

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load

# source ~/.config/zsh/two-line
# export PROMPT_CHAR="❯"
# autoload -Uz add-zsh-hook
# _pista_prompt() {
# 	PROMPT="$(pista -z)"   # `pista -zm` for the miminal variant
# }
# add-zsh-hook precmd _pista_prompt

HISTFILE="$HOME/.zsh_history"
HISTSIZE=2000
SAVEHIST=2000
setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY               # Don't execute immediately upon history expansion.
setopt HIST_BEEP                 # Beep when accessing nonexistent history.
bindkey -v



take () { mkdir $1 && cd $1 }

alias vim="nvim"
alias tmux="tmux -2" # force using 256 color
alias cwd="pwd | pbcopy"
alias please="sudo"
alias difff="git diff --color | diff-so-fancy"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh


# HONOR SPECIFIED {{
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export JAVA_HOME=$(/usr/libexec/java_home)

export PATH="/usr/local/opt/mysql@5.7/bin:$PATH"
export PATH="$HOME/bin:$PATH"
# export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"

export ITERM_ENABLE_SHELL_INTEGRATION_WITH_TMUX=1

function add_thrift_path {
    # If we're not in a Git repo, then there's no work to do
    git rev-parse 2>/dev/null || return

    REPO_ROOT=$(git rev-parse --show-toplevel)
    THRIFT_PATH="$REPO_ROOT/thrift/out/gen-py"

    # If this Git repo doesn't use a Thrift submodule, remove any old Thrift from the path
    if [ ! -d $THRIFT_PATH ]; then
        #echo "Not any Thrift repo"
        export PYTHONPATH=$(echo $PYTHONPATH | sed 's@^[^:]*thrift/out/gen-py@@')
    # There's a Thrift module! Add it to the path
    elif [[ $PYTHONPATH == *"thrift/out/gen-py"* ]]; then
        #echo "Existing"
        export PYTHONPATH=$(echo $PYTHONPATH | sed 's@^[^:]*thrift/out/gen-py@'"$THRIFT_PATH"'@')
    else
        #echo "Was empty"
        export PYTHONPATH="$THRIFT_PATH${PYTHONPATH+":"}${PYTHONPATH-}"
    fi
}

function honor_repo {
    cd "$HOME/work/$1"
    add_thrift_path
}

function h4-kill {
    kill -9 $(lsof -i:8080)
    kill -9 $(lsof -i:4000)
    kill -9 $(lsof -i:5000)
    kill -9 $(lsof -i:3010)
}

alias h4-web='honor_repo external-web'
alias h4-api='honor_repo external-api'
alias h4-thrift='honor_repo thrift'
alias h4-server-config='honor_repo server-config'
alias h4-salt='ssh bastion ssh saltmaster sudo journalctl --follow --lines=10000 | rg pull-and-run'
alias sona-db="mycli --user sonasandbox --password=sona --database='sonasandboxdb'"
alias tree="br"
alias ls="exa --git"
alias g-switch-branch="git branch --list --color=always | fzf --ansi | sed 's/\*//g' | xargs git checkout"
alias g-select-hash="git dog --color=always | fzf --ansi | awk '{print \$2}'"
alias g-fixup="g-select-hash | xargs git commit --fixup"
alias g-hash="g-select-hash | pbcopy"
alias g-conflig="rg '^<<<<<<<' ; rg '======$'"
alias n-e="nvim ~/work/docs/todo.rst"
alias n-b="bat ~/work/docs/todo.rst"

# }}
# }
export VISUAL=nvim
export EDITOR="$VISUAL"
export FZF_DEFAULT_COMMAND="fd --type file --color=always"
export FZF_DEFAULT_OPTS="--ansi"

function issue {
    open "https://github.com/joinhonor/tracker/issues/$1"
}

function side-by-side {
    local title=$1
    local leftLabel=$2
    local rightLabel=$3
    local left=$4
    local right=$5
    local out=$6
    montage -geometry 100% -frame 2 \
        -frame 5 \
        -title "$title" \
        -label "$beforeLabel" $before \
        -label "$afterLabel"  $after \
        $out
}
function before-after {
    local before=$1
    local after=$2
    local out=$3
    side-by-side "Before v After" "Before" "After" $before $after $out
}

function find-and-replace {
    local sed_command=$1
    local file_type=$2
    find . -name "$file_type" -exec sed -i '' "'$sed_command'" "{}" \;
}

function name-tab {
    shtuff as "'$1'"
    echo -ne "\033];$1\007"
}

g-review-new() {
  local commits commit
  commits=$(git log --pretty=oneline --color=always  --abbrev-commit --reverse) &&
  commit=$(echo "$commits" | fzf --tac +s +m -e | sed "s/ .*//") &&
  echo "rbt-log-post --open --parent $commit~ $commit"
}


source /Users/isthisnagee/Library/Preferences/org.dystroy.broot/launcher/bash/br

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"
export NVM_DIR="$HOME/.nvm"
  [ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
  # [ -s "/usr/local/opt/nvm/etc/bash_completion" ] && . "/usr/local/opt/nvm/etc/bash_completion"  # This loads nvm bash_completion
  #
eval $(thefuck --alias)

# theme!
source ~/.config/zsh/purepower
# eval "$(starship init zsh)"
